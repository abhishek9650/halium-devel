#!/bin/bash

##Setup the environment
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install java
mkdir hybris && cd hybris

pwd
repo init -u git://github.com/mer-hybris/android.git -b hybris-14.1
mkdir -p .repo/local_manifests
cp ../wt88047.xml .repo/local_manifests/
cp ../build.sh /opt/atlassian/pipelines/agent/build/hybris
repo sync -c -j 32

